#!/usr/bin/env python
# -*- coding: utf-8 -*-

from coltrans import decrypt as col_decrypt
from caesar2 import decrypt as cae_decrypt

# File: msg3.enc, algorithm: unknown, key: 14.
steps = """
steps:
with numeric key, try Columnar and Caesar
"""

cipher = "".join(open("enc_msgs/msg3.enc").readlines()).upper()
print "Derypted with columnar:\n{}".format(col_decrypt(cipher, 14))
print
print "Derypted with ceasar:\n{}".format(cae_decrypt(cipher, 14))
print steps

