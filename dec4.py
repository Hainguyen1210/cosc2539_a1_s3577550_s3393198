#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subst

# File: msg4.enc, algorithm: Random Substitution, key: unknown.
# at first i try to brute force with 2 threads normal one and the reverse, but it seems cost a lot of time
# then i try to guess it based on the letter frequency
steps = """
steps:
get the cipher's letter frequency
>1$';lazry(,bvgni)ep:"sju0q5-okf?><4\< \ represents new line character in this explaining case
compare with the normal English frequency (http://letterfrequency.org/)
use the two above then tried to substitute letters, guess the possible words
comparing the results with the cipher in order to guess possible letters
replace "_" with possible letters in guess_letter_frequency

> e__________________________________< i started with this because SPACE and E are the 2 most used letters
then filter 3 letters words which maybe is "The" - the most used word
> e___t_____h________________________< i
keep trying words like "all", "at", "that", "for", "our", "your", "voice", "devices", "and", "follow", "today"
> eaortsniclhudywmpbfgkv______z____x_<
found most of the words and i google for the whole sentence then i got all the rest including special characters
> eaortsniclhudywmpbfgkv.,-'"(z:)><x4<
"""


def checkCharFrequent(text):
    """
    check the frequency of characters in a string
    :param text: input string
    :return: a list of char and frequency
    """
    result = []
    for char1 in text:
        apears = 0
        for char2 in text:
            if char1 == char2:
                apears += 1
        if apears > 0:
            result.append([apears, char1])
        text = text.replace(char1, "")
        result = sorted(result, reverse=True)
    return result


def checkWordFrequency(text):
    words = text.split(" ")
    result = []
    for word1 in words:
        frequency = 0
        exist = False
        for word2 in words:
            if word2 == word1:
                frequency += 1
        for ele in result:
            if ele[1] == word1:
                exist = True
        if not exist: result.append([frequency, word1])
    result = sorted(result, reverse=True)
    return result


def produce_key(guess_freq, cipher_freq):
    # put all the letters which was not counted
    for character1 in alphabet.lower():
        if character1 not in cipher_freq:
            cipher_freq += character1
        if character1 not in guess_freq:
            guess_freq += character1

    key = ""
    for character2 in alphabet.lower():
        index = guess_freq.find(character2)
        if index != -1:
            subs_character2 = cipher_freq[index]
            key += subs_character2
    return key


def randomsubs_unkown_key(cipher, guess_letter_frequency):
    current_cipher_letter_frequency = checkCharFrequent(cipher)
    cipher_letter_frequency = ""
    decrypted = ""
    # try to substitute based on most frequent characters
    for i in range(len(guess_letter_frequency)):
        cipher_letter_frequency += current_cipher_letter_frequency[i][1]
    guess_letter_frequency = guess_letter_frequency[:len(cipher_letter_frequency)]
    for character in cipher:
        if character in cipher_letter_frequency:
            index = cipher_letter_frequency.index(character)
            subs_character = guess_letter_frequency[index]
            decrypted += subs_character.upper()
        else:
            decrypted += character.lower()

    ordered_cipher_lf = "".join(cipher_letter_frequency).lower()
    print "cipher's most frequent character: >{}<".format(ordered_cipher_lf)
    print "   guess most frequent character: >{}<".format(guess_letter_frequency)
    print cipher.lower(),
    print decrypted

    # a = checkWordFrequency(decrypted)
    # for b in a:
    #     if len(b[1]) == 3:
    #         print b

    final_key = ""
    if "_" not in guess_letter_frequency:
        final_key = produce_key(guess_letter_frequency, ordered_cipher_lf)
    print "key: >{}<".format(final_key)
    print "re_check: ",
    print subst.decrypt(cipher.upper(), final_key.upper())

alphabet = """ABCDEFGHIJKLMNOPQRSTUVWXYZ .,:;()-!?$'"\n0123456789"""
if __name__ == '__main__':
    guess_frequency = """ eaortsniclhudywmpbfgkv.,-'"(z:)><x4"""
    text_in = "".join(open("enc_msgs/msg4.enc").readlines())
    randomsubs_unkown_key(text_in, guess_frequency)
    print steps
