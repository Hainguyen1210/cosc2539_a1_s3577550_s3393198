#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
import sys


def clean_text(string, valid_string):
    """
    remove characters not in the valid string
    :param string: input text String
    :param valid_string: a String of all the valid characters
    :return: output text without characters not in "valid_string" String
    """
    for i in string:
        if i not in valid_string:
            string = string.replace(i, "")
    return string


def write_output(string, file_name):
    f = open(file_name, "w")
    f.write(string)
    f.close()


def encrypt(message, key):
    """
    encrypt using random substitution
    :param message: a string needed to be encrypted
    :param key: a key string
    :return: encrypted string
    """
    encrypted = ""
    try:
        message = clean_text(message.upper(), alphabet)
        for char in message:
            index = alphabet.find(char)
            encrypted += key[index]
    except IOError, e1:
        print e1
    return encrypted


def decrypt(cipher, key):
    """
    decrypt using random substitution
    :param cipher: a string needed to be decrypted
    :param key: a key string
    :return: decrypted string
    """
    decrypted = ""
    cipher = clean_text(cipher.upper(), alphabet)
    for char in cipher:
        index = key.find(char)
        decrypted += alphabet[index]
    return decrypted


alphabet = """ABCDEFGHIJKLMNOPQRSTUVWXYZ .,:;()-!?$'"\n0123456789"""
if __name__ == "__main__":
    # get arguments
    print str(sys.argv)
    try:
        mode = sys.argv[1]
        if mode != "g":
            inputFileName = sys.argv[2]
            keyFileName = sys.argv[3]
        else:
            keyFileName = sys.argv[2]
    except IndexError, e:
        print "lack of options -mode -filename -key"
        sys.exit()

    if mode == "g":
        key = "".join(random.sample(alphabet, len(alphabet)))
        write_output(key, keyFileName)
        print "key generated"
    elif mode in "ed":
        try:
            inputText = "".join(open(inputFileName, "r").readlines()).upper()
            inputKey = "".join(open(keyFileName, "r").readlines())
        except IOError, e:
            print e
        # navigate
        if mode == "e":
            print "encryption",
            out = encrypt(inputText, inputKey)
            formatPosition = inputFileName.find(".")
            outputFileName = inputFileName[:formatPosition] + "_subs_encrypted" + inputFileName[formatPosition:]
            write_output(out, outputFileName)
            print "completed"
        elif mode == "d":
            print "decryption",
            out = decrypt(inputText, inputKey)
            # write output
            formatPosition = inputFileName.find(".")
            outputFileName = inputFileName[:formatPosition] + "_subs_decrypted" + inputFileName[formatPosition:]
            outputFileName = outputFileName.replace("_subs_encrypted", "")
            write_output(out, outputFileName)
            print "completed"
    else:
        print "incorrect option"

