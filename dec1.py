#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dec2 import validate_eng
from caesar2 import decrypt

# File: msg1.enc, algorithm: Ceasar, key: unknown.
# result: >UFRESH OFF THE LAUNCH OF THE GOOGLE PIXEL, THE NEW GOOGLE HARDWARE DIVISION IS BACK WITH ITS SECOND PRODUCT:
# GOOGLE HOME, A VOICE COMMAND APPLIANCE AND GOOGLE CAST-ENABLED SPEAKER. THERE IS BASICALLY NO INTERFACE AT ALL TO THIS
# PRODUCT--IT'S ALL VOICE COMMANDS, ALL THE TIME. I HOPE YOU LIKE SPEAKING TO YOUR ELECTRONICS.UK<
steps = """
steps:
try all the keys - 50
filter all the results by validating English words
(get the 5000 English words at www.wordfrequency.info, using python and text editor)
"""

alphabet = """ABCDEFGHIJKLMNOPQRSTUVWXYZ .,:;()-!?$'"\n0123456789"""


def caesar_unknown_key(cipher):
    decrypted_list = []
    for i in range(len(alphabet)):
        decrypted = decrypt(cipher, i)
        decrypted_list.append(decrypted)

    # filter results
    eng_words = open("5000CommonWords.txt").read().replace("\n", " ")
    eng_words = eng_words.split(" ")
    messages = validate_eng(eng_words, decrypted_list, 0.4, 0)
    for message in messages:
        print ">{}<".format(message)
if __name__ == '__main__':
    text = "".join(open("enc_msgs/msg1.enc").readlines()).upper()
    for i in "<>":
        text = text.replace(i, "")
    caesar_unknown_key(text)

    print steps