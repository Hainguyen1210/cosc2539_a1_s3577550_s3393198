#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
import sys

from subst import write_output


def encrypt(text_in, e_key, key_offset, do_print=0):
    """
    encrypt message with the Vernam cipher method
    :param text_in: the message String
    :param e_key: the key String
    :param key_offset: the position to read the key from String
    :param do_print: 0,1 to indicate whether to print out the process Integer
    :return: cipher/encrypted message String
    """
    encrypted = ""
    for char in text_in:
        key_char = e_key[key_offset]
        key_offset += 1
        char_num = alphabet.find(char)
        key_num = alphabet.find(key_char)
        cip_num = (char_num + key_num) % len(alphabet)
        cip_char = alphabet[cip_num]
        encrypted += cip_char
    if do_print == 1:print "next offset {}".format(key_offset)
    return encrypted


def decrypt(text_in, d_key, key_offset, do_print=0):
    """
    decrypt cipher with the Vernam cipher method
    :param text_in: the cipher String
    :param d_key: the key String
    :param key_offset: the position to read the key from String
    :param do_print: 0,1 to indicate whether to print out the process Integer
    :return: message/decrypted message String
    """
    decrypted = ""
    for char in text_in:
        key_char = d_key[key_offset]
        key_offset += 1
        char_num = alphabet.find(char)
        key_num = alphabet.find(key_char)
        cip_num = (char_num - key_num) % len(alphabet)
        cip_char = alphabet[cip_num]
        decrypted += cip_char
    if do_print == 1:print "next offset {}".format(key_offset)
    return decrypted


alphabet = """ABCDEFGHIJKLMNOPQRSTUVWXYZ .,:;()-!?$'"\n0123456789"""
if __name__ == "__main__":
    # get arguments

    print str(sys.argv)
    try:
        mode = sys.argv[1]
        if mode not in "edg":
            print "modes are: e d g"
            sys.exit()
        if mode != "g":
            try:
                inputFileName = sys.argv[2]
                keyFileName = sys.argv[3]
                offset = int(sys.argv[4])
            except IndexError, e:
                print "need -inputFileName -keyFileName -offset"
                sys.exit()
        else:
            try:
                keyFileName = sys.argv[2]
                keyLength = int(sys.argv[3])
            except IndexError, e:
                print "key generation mode need -keyFileName -keyLength"
                sys.exit()
    except IndexError, e:
        print "Please specify mode"
        sys.exit()
    except ValueError, e:
        print "keyLength/offset is an integer"
        sys.exit()

    # key generation mode
    if mode == "g":
        key = ""
        for i in range(keyLength):
            key += random.choice(alphabet)
            print key
        write_output(key, keyFileName)
        print "key generated"
    elif mode in "ed":
        # get data from files and check whether key's length is enough
        try:
            inputText = "".join(open(inputFileName, "r").readlines()).upper()
            inputKey = "".join(open(keyFileName, "r").readlines())
            message_len = len(inputText)
            key_len = len(inputKey)

            if message_len > key_len - offset:
                print "key's length is not enough"
                print "message: {}\nkey : {}\noffset: {}\nneed {} more".\
                    format(message_len, key_len, offset, message_len - (key_len - offset))
                sys.exit()
        except IOError, e:
            print e

        # navigate to encryption or decryption mode
        if mode == "e":
            out = encrypt(inputText, inputKey, offset, 1)
            # write output to file
            formatPosition = inputFileName.find(".")
            outputFileName = inputFileName[:formatPosition] + "_vern_encrypted" + inputFileName[formatPosition:]
            write_output(out, outputFileName)
            print "encryption completed"
        elif mode == "d":
            out = decrypt(inputText, inputKey, offset, 1)
            # write output to file
            formatPosition = inputFileName.find(".")
            outputFileName = inputFileName[:formatPosition] + "_vern_decrypted" + inputFileName[formatPosition:]
            outputFileName = outputFileName.replace("_vern_encrypted", "")
            write_output(out, outputFileName)
            print "decryption completed"
