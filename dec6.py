#!/usr/bin/env python
# -*- coding: utf-8 -*-


from caesar2 import decrypt as cae_decrypt
from coltrans import decrypt as col_decrypt
from dec2 import validate_eng
from dec4 import checkCharFrequent

# File: msg6.enc, algorithm: Columnar Transposition first, then Ceasar, key: unknown,
# but same key for both encryption steps.
steps="""
steps:
find factors for Columnar but it must be less than alphabet's length (for Caesar)
==> failed because there a "trick" in cipher's length
try caesar with all key from 1-51
filter those results by letter ratio and letter frequency to narrow the results
( we can get the key by adjust letter ratio and look at letter frequency which starts with " eta..")
decrypt results with columnar with the same keys
detect English words
"""


def cal_letter_ratio(text):
    """
    check the percentage of letter in the string
    :param text: input text contains mixed with all characters String
    :return: percentage of letter in the message Float
    """
    total = len(text)
    total_letter = 0
    for char in text:
        if char in "ABCDEFGHIJKLMNOPQRSTUVWXYZ ":
            total_letter += 1
    return float(total_letter)/total


alphabet = """ABCDEFGHIJKLMNOPQRSTUVWXYZ .,:;()-!?$'"\n0123456789"""
if __name__ == '__main__':
    # get cipher's length
    cipher = "".join(open("enc_msgs/msg6.enc").readlines())
    for i in "<>":
        cipher = cipher.replace(i, "")
    length = len(cipher)
    print length

    # get factors (factors do not work in this case since there is problem in cipher's length
    # factors = []
    # for i in range(1, length):
    #     if length % i == 0:
    #         factors.append(i)
    # print factors

    results = []
    # try Caesar with all the possible keys
    print "with the letter ratio > 0.8: "
    for i in range(1,51):
        cae_decrypted = cae_decrypt(cipher, i)
        letter_ratio = cal_letter_ratio(cae_decrypted)
        # detects sentences that make sense by "A-Z" and SPACES
        if letter_ratio > 0.8:
            # check char frequency
            cipher_letter_frequency = ""
            list_cipher_letter_frequency = checkCharFrequent(cae_decrypted)
            for j in range(len(list_cipher_letter_frequency)):
                cipher_letter_frequency += list_cipher_letter_frequency[j][1]
            print "key: {} frequency >{}<".format(i, cipher_letter_frequency)
            # calculate columnar with both directions
            results.append(col_decrypt(cae_decrypted, i))
            results.append(col_decrypt(cae_decrypted, len(cipher)/i))

    print "----------------\ndecrypted with columnar\nfiltering results with spelling"
    eng_words = open("5000CommonWords.txt").read().replace("\n", " ")
    eng_words = eng_words.split(" ")
    messages = validate_eng(eng_words, results, 0.4, 0)
    for message in messages:
        print message
    print steps
