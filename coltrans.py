#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from subst import clean_text
from subst import write_output


def is_prime(num):
    for i in range(2, num):
        if num % i == 0:
            return False
    return True


def encrypt(message, en_key):
    """
    encrypt message by columnar transposition method
    :param message: plain text/message String
    :param en_key: Integer shows the number of columns
    :return: cipher/encrypted message String
    """
    message = clean_text(message.upper(), alphabet)
    col = en_key
    row = len(message)/col if len(message)%col < 1 else len(message)/col+1
    length = col * row
    message += " " * (length - len(message))
    table = []
    # columnar trans
    for i in range(row):
        table.append([])
    # put all the characters in the table
    for i in range(col):
        for j in range(row):
            table[j].append(message[:1])
            message = message[1:]
    # read all the characters from the table but with the other direction
    encrypted = ""
    for i in range(row):
        for j in range(col):
            encrypted += table[i][j]
    return encrypted


def decrypt(cipher, de_key):
    """
    decrypt message by columnar transposition method
    :param cipher: cipher string to decrypt String
    :param de_key: an Integer shows the number of columns
    :return: message String
    """
    cipher = clean_text(cipher.upper(), alphabet)
    row = de_key
    col = len(cipher)/row
    table = []
    for i in range(row):
        table.append([])
    for i in range(col):
        for j in range(row):
            table[j].append(cipher[:1])
            cipher = cipher[1:]

    decrypted = ""
    for i in range(row):
        for j in range(col):
            decrypted += table[i][j]
    return decrypted


alphabet = """ABCDEFGHIJKLMNOPQRSTUVWXYZ .,:;()-!?$'"\n0123456789"""
if __name__ == "__main__":
    # get the input
    try:
        mode = sys.argv[1]
        inputFileName = sys.argv[2]
        key = int(sys.argv[3])
    except IndexError, e1:
        print "require mode - inputFileName - key"
        sys.exit()
    except ValueError, e2:
        print "key is an integer"
        sys.exit()

    try:
        inputText = "".join(open(inputFileName))

        # navigate
        if mode == "e":
            print "encryption",
            out = encrypt(inputText, key)
            formatPosition = inputFileName.find(".")
            outPutFile = inputFileName[:formatPosition] + "_col_encrypted" + inputFileName[formatPosition:]
            write_output(out, outPutFile)

            print "completed"
        elif mode == "d":
            print "decryption",
            out = decrypt(inputText, key)
            formatPosition = inputFileName.find(".")
            outPutFile = inputFileName[:formatPosition] + "_col_decrypted" + inputFileName[formatPosition:]
            outPutFile = outPutFile.replace("_col_encrypted", "")
            write_output(out, outPutFile)
            print "completed"
    except IOError, e:
        print e
        sys.exit()

