#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from coltrans import write_output


def encrypt(text, e_key):
    """
    encrypt message using caesar method
    :param text: a plain text String
    :param e_key: encrypt key is an Integer
    :return: encrypted message String
    """
    encrypted = ""
    for char in text:
        position = alphabet.find(char)
        subs_char = alphabet[(position+e_key)%len(alphabet)]
        encrypted += subs_char
    return encrypted


def decrypt(text, d_key):
    """
    reversion of encrypt
    :param text: cipher/encrypted message String
    :param d_key: decrypt key is a Integer
    :return: message String
    """
    decrypted = ""
    for char in text:
        position = alphabet.find(char)
        al_length = len(alphabet)
        subs_char = alphabet[(position - d_key) % len(alphabet)]
        decrypted += subs_char
    return decrypted


alphabet = """ABCDEFGHIJKLMNOPQRSTUVWXYZ .,:;()-!?$'"\n0123456789"""
if __name__ == '__main__':
    file_in = ""
    file_out = ""
    text_in = ""
    text_out = ""
    key = 0
    try:
        # get inputs
        mode = sys.argv[1]
        if mode not in "de":
            print 'mode must be e or d'
            sys.exit()
        file_in = sys.argv[2]
        key = int(sys.argv[3])
        if key > len(alphabet) or key < 0:
            print 'key must be between: {} and {}'.format(0, len(alphabet))
            sys.exit()
        text_in = "".join(open(file_in).readlines()).upper()

        # navigate
        if mode == "e":
            print "encryption",
            text_out = encrypt(text_in, key)
            formatPosition = file_in.find(".")
            file_out = file_in[:formatPosition] + "_cea_encrypted" + file_in[formatPosition:]
            print "completed"

        elif mode == "d":
            print "decryption",
            text_out = decrypt(text_in, key)
            formatPosition = file_in.find(".")
            file_out = file_in[:formatPosition] + "_cea_decrypted" + file_in[formatPosition:]
            file_out = file_out.replace("_cea_encrypted", "")
            print "completed"

        write_output(text_out, file_out)

    except IOError, e:
        print e
        sys.exit()
    except IndexError, e:
        print "lack of options -mode -filename -key"
        sys.exit()
