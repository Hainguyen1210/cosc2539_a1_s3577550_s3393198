#!/usr/bin/env python
# -*- coding: utf-8 -*-

import coltrans
# File: msg2.enc, algorithm: Columnar Transposition, key: unknown.
# result: >HERE AT ARS, WE'RE ALWAYS MAKING LISTS (JUST LIKE LIAM NEESON). LISTS OF SCIENCE FICTION MOVIES ARE A COMMON
# ITEM FOR DISCUSSION ON THE ARS STAFF SLACK CHANNE--PARTICULARLY SHORT LISTS OF THE BEST SCIENCE FICTION MOVIES EVER
# MADE. BUT "BEST" IS AN IMPOSSIBLE WORD TO QUANTIFY IN ANY BROADLY APPLICABLE WAY--ONE PERSON'S "BEST EVER" MIGHT BE
# ANOTHER PERSON'S WORST, ESPECIALLY IN A GENRE OF MOVIES AS RICH AND VARIED AS SCIENCE FICTION.<
steps= """
steps:
calculated the length of the cipher
calculate all the possible factors
run decryption with all the factors
filter all the results by validating English words
(get the 5000 English words at www.wordfrequency.info, using python and text editor)
"""


def validate_eng(eng_words, target_list, ratio, doPrint):
    """
    check all the targets to find which ones are english
    :param eng_words: list of english words
    :param target_list: a list of targets needed to be checked
    :param ratio: a number 0 - 1 show how correct the sentence is
    :param doPrint: 1-print out, 0-not print
    :return: a list of valid message
    """
    valid = []
    if doPrint: print "Number of words in dictionary: {}".format(len(eng_words))

    for target in target_list:
        words = target.split(" ")
        invalid = 0
        for word in words:
            # clear special characters
            for char in ",.()":
                word = word.replace(char, "")
            word = word.lower()
            if doPrint: print word,
            if word == " ":
                words.remove(word)
            else:
                if word not in eng_words:
                    invalid += 1
        wordRatio = float(invalid)/len(words)
        if doPrint: print "\ninvalid: {} / total: {} = {}".format(invalid, len(words), wordRatio)
        if wordRatio < ratio:
            valid.append(target)
    return valid


def col_unkown_key(cipher):
    # get cipher's length
    length = len(cipher)

    # get factors
    factors = []
    results = []
    for i in range(1, length):
        if i > 1:
            if length % i == 0:
                factors.append(i)

    # generate results based on factors
    for i in factors:
        out = coltrans.decrypt(cipher, i)
        results.append(out)

    print "cipher's length: {} \nnumber of decrypted: {} \nResult list:".format(length, len(results))

    # filter results
    eng_words = open("5000CommonWords.txt").read().replace("\n", " ")
    eng_words = eng_words.split(" ")
    valid_results = validate_eng(eng_words, results, 0.4, 0)
    for valid_result in valid_results:
        print ">{}<".format(valid_result)


if __name__ == '__main__':
    text = "".join(open("enc_msgs/msg2.enc").readlines())
    for i in "<>":
        text = text.replace(i, "")
    col_unkown_key(text)
    print steps
