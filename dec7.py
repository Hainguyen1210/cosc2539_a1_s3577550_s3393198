#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dec1 import caesar_unknown_key
from dec2 import col_unkown_key
from dec4 import randomsubs_unkown_key

# File: msg7.enc, algorithm: unknown but not Vernam, key: unknown.
steps = """
steps:
try Caesar (not this one)
try Columnar (not this one)
try random substitution (this one again then i used the same method as decrypting msg4.enc)
>?9rqil6!3ch ks8'e(w4dvp2ygzfxjmotau.,;:n)-0$"1b\n57<
"""

if __name__ == '__main__':
    text = "".join(open("enc_msgs/msg7.enc").readlines()).upper()
    for i in "<>":
        text = text.replace(i, "")
    print "--try using Caesar:"
    caesar_unknown_key(text)
    print "\n--try using Columnar:"
    col_unkown_key(text)
    print "\n--try using Random substitution:"
    # guess_frequency ="""zi?84s3(w !rqkl'vdxp96yfahng;b210"""
    guess_frequency = """ eaotnirslhcdmfpvu,wbgy.-k\nz'6x52"""
    randomsubs_unkown_key(text, guess_frequency)
    print steps
