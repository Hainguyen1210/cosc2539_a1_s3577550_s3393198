#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dec2 import validate_eng
from vernam2 import decrypt

steps = """
steps:
results from msg1 and msg2 are combined to make a key (msg1+msg2)
using Vernam with all the possible offset
filter the results with dictionary
then i find out that you encrypted it with different offsets for different parts
print decrypt(msg5, key, 1)[:len(msg1)],
print decrypt(msg5, key, 0)[len(msg1):],
"""
msg1 = """FRESH OFF THE LAUNCH OF THE GOOGLE PIXEL, THE NEW GOOGLE HARDWARE DIVISION IS BACK WITH ITS SECOND PRODUCT: GOOGLE HOME, A VOICE COMMAND APPLIANCE AND GOOGLE CAST-ENABLED SPEAKER. THERE IS BASICALLY NO INTERFACE AT ALL TO THIS PRODUCT--IT'S ALL VOICE COMMANDS, ALL THE TIME. I HOPE YOU LIKE SPEAKING TO YOUR ELECTRONICS."""
msg2 = """HERE AT ARS, WE'RE ALWAYS MAKING LISTS (JUST LIKE LIAM NEESON). LISTS OF SCIENCE FICTION MOVIES ARE A COMMON ITEM FOR DISCUSSION ON THE ARS STAFF SLACK CHANNE--PARTICULARLY SHORT LISTS OF THE BEST SCIENCE FICTION MOVIES EVER MADE. BUT "BEST" IS AN IMPOSSIBLE WORD TO QUANTIFY IN ANY BROADLY APPLICABLE WAY--ONE PERSON'S "BEST EVER" MIGHT BE ANOTHER PERSON'S WORST, ESPECIALLY IN A GENRE OF MOVIES AS RICH AND VARIED AS SCIENCE FICTION.               """

# msg1 = "".join(open("enc_msgs/msg1.enc"))
# msg2 = "".join(open("enc_msgs/msg2.enc"))
msg5 = "".join(open("enc_msgs/msg5.enc"))

for char in "><":
    msg1 = msg1.replace(char, "")
    msg2 = msg2.replace(char, "")
    msg5 = msg5.replace(char, "")

key = msg1 + msg2
print len(msg1)
print len(msg2)
print len(key)
print len(msg5)
max_offset = len(key)-len(msg5)
results = []
for i in range(0, max_offset):
    decrypted = decrypt(msg5, key, i)
    results.append(decrypted)

# filter results
eng_words = open("5000CommonWords.txt").read().replace("\n", " ")
eng_words = eng_words.split(" ")
valid_results = validate_eng(eng_words, results, 0.4, 0)

print "-------possible results---------"
for valid_result in valid_results:
    print ">{}<".format(valid_result)

print "++++try your way+++++"
print decrypt(msg5, key, 1)[:len(msg1)],
print decrypt(msg5, key, 0)[len(msg1):],

print steps
